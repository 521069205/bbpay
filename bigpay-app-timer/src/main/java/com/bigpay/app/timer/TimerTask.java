package com.bigpay.app.timer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bigpay.app.timer.biz.ReportBiz;
import com.bigpay.app.timer.utils.SpringContextUtil;
import com.bigpay.common.core.utils.DateUtils;

public class TimerTask {
	
	private static final Log LOG = LogFactory.getLog(TimerTask.class);
	
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			// 加载Spring配置文件
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
					new String[] { "spring-context.xml" });
			// 初始化SpringContextUtil
			final SpringContextUtil ctxUtil = new SpringContextUtil();
			ctxUtil.setApplicationContext(context);
			ReportBiz reportBiz = (ReportBiz) SpringContextUtil
					.getBean("reportBiz");
			
			Date reportDate = DateUtils.addDay(new Date(), -1);
			
			// 生成报表
			reportBiz.createReport(sdf.format(reportDate));
			

		} catch (Exception e) {
			LOG.error("bigpay-app-timer error:", e);
		}
	}
}
