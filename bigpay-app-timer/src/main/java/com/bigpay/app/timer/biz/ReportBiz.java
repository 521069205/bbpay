package com.bigpay.app.timer.biz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bigpay.report.service.RpDayReportService;

/**
 * 报表biz.
 */
@Component("reportBiz")
public class ReportBiz {

	private static final Log LOG = LogFactory.getLog(ReportBiz.class);
	
	@Autowired
	private RpDayReportService rpDayReportService;

	/**
	 * 创建报表
	 */
	public void createReport(String collectDate) {
//		collectDate = "2017-03-17";
		LOG.info("报表定时任务开始,collectDate："+collectDate);
		rpDayReportService.createReport(collectDate);
		
		LOG.info("报表定时任务结束");
	}
}
