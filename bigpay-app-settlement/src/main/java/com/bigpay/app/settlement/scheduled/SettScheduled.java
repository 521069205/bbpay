package com.bigpay.app.settlement.scheduled;


/**
 * 结算定时任务接口.
 */
public interface SettScheduled {

	/**
	 * 发起每日待结算数据汇总.
	 */
	public void launchDailySettCollect();
	
	/**
	 * 发起定期自动结算.
	 */
	public void launchAutoSett();

}
