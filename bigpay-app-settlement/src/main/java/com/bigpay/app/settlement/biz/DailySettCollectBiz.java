package com.bigpay.app.settlement.biz;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bigpay.account.entity.RpAccount;
import com.bigpay.account.service.RpSettHandleService;
import com.bigpay.user.entity.RpUserPayConfig;
import com.bigpay.user.service.RpUserPayConfigService;

/**
 * 每日待结算数据汇总.
 */
@Component("dailySettCollectBiz")
public class DailySettCollectBiz {

	private static final Log LOG = LogFactory.getLog(DailySettCollectBiz.class);

	@Autowired
	private RpSettHandleService rpSettHandleService;
	
	@Autowired
	private RpUserPayConfigService rpUserPayConfigService;

	/**
	 * 按单个商户发起每日待结算数据统计汇总.<br/>
	 * 
	 * @param userEnterprise
	 *            单个商户的结算规则.<br/>
	 * @param endDate
	 *            统计日期 ==定时器执行的日期<br/>
	 */
	public void dailySettCollect(RpAccount rpAccount, Date endDate) {
		LOG.info("按单个商户发起每日待结算数据统计汇总");
		RpUserPayConfig rpUserPayConfig = rpUserPayConfigService.getByUserNo(rpAccount.getUserNo());
		if (rpUserPayConfig == null) {
			LOG.info("userNo:" + rpAccount.getUserNo() + ":没有商家设置信息，不进行汇总");
			return;
		}
		int riskDay = rpUserPayConfig.getRiskDay();
		rpSettHandleService.dailySettlementCollect(rpUserPayConfig.getUserNo(), endDate, riskDay,rpUserPayConfig.getUserName());
	}
}
